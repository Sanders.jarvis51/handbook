---
title: "Plan Stage Engineering Metrics"
---

## Group Pages

- [Knowledge Group Dashboards](/handbook/engineering/metrics/dev/plan/knowledge)
- [Project Management Group Dashboards](/handbook/engineering/metrics/dev/plan/project-management)
- [Product Planning Group Dashboards](/handbook/engineering/metrics/dev/plan/product-planning)
- [Optimize Group Dashboards](/handbook/engineering/metrics/dev/plan/optimize)

{{% engineering/child-dashboards stage=true filters="Plan" %}}
